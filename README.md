This project contains the code for a basic implementation of the Spaces app using Node.js & express by Haley Archer-McClellan

Build instructions:

$ node server.js

Release notes:
- the API mock is hosted at https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/
- the spec as given did not return data for the spaces endpoint. I fixed this and updated the file.
