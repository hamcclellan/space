const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const axios = require('axios');
var path = require('path');
const app = express()
app.use(bodyParser.json())

app.use(express.static(path.resolve(__dirname)))
var helpers = require('utils');

app.get('/', function (req, res) {

    res.sendFile('index.html', { root: __dirname });

});


app.get('/space',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space/'+req.params.id)
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/spaces',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space')
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/users',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/users')
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/user',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/users/'+req.params.id)
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/entries',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space/'+req.params.spaceId+'/entries')
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/entry',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space/'+req.params.spaceId+'/entries/'+req.params.entryId)
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/assets',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space/'+req.params.spaceId+'/assets')
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});
app.get('/assets',function(req,res){
  axios.get('https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space_api/1.0.0/m/space/'+req.params.spaceId+'/assets/'+req.params.assetId)
  .then((response) => { res.send(response.data)})
  .catch((err) => { console.log(err)})
});



app.post('/', function (req, res) {

})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
